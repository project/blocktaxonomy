CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module has a block to display the menu of the taxonomy


REQUIREMENTS
------------

* This module don't need requirements because Taxonomy module is inside Drupal core.


INSTALLATION
------------

 * Install the Block Taxonomy module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420
   for further information.


CONFIGURATION
-------------

 * Place the block on a page, it will render a menu with the registered taxonomies.


MAINTAINERS
-----------

 * Ilgner Fagundes (ilgnerfagundes) - https://www.drupal.org/u/ilgnerfagundes

Supporting organization:

 * CI&T - https://www.drupal.org/cit
