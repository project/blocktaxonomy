<?php

namespace Drupal\filter_taxonomy\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Display a taxonomy block.
 *
 * @Block(
 *   id = "filter_taxonomy",
 *   admin_label = @Translation("Filter Taxonomy"),
 * )
 */
class FilterTaxonomy extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructor class __construct.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $request_stack;
  }

  /**
   * Create container create.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $arr = explode('?', $this->requestStack->getCurrentRequest()->getRequestUri());

    $current_uri = $arr[0];

    $vid = 'blog';
    $term_data = [];
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $slug = $this->slugify($term->name);
      $link = "/blog/{$slug}";

      $url_full = "{$link}?category={$term->tid}";
      $is_active = FALSE;
      if ($link == $current_uri) {
        $is_active = TRUE;
        $url_full = '/blog';
      }

      $term_data[] = [
        'id' => $term->tid,
        'name' => $term->name,
        'link' => $url_full,
        'is_active' => $is_active,
      ];
    }
    return [
      '#theme' => 'filter_taxonomy',
      '#taxsonomies' => $term_data,
    ];
  }

  /**
   * Slugfy name.
   *
   * @param string $string
   *   The current name.
   *
   * @return string
   *   The current name slugfy.
   */
  public static function slugify($string) {
    $table = [
      'Š' => 'S',
      'š' => 's',
      'Đ' => 'Dj',
      'đ' => 'dj',
      'Ž' => 'Z',
      'ž' => 'z',
      'Č' => 'C',
      'č' => 'c',
      'Ć' => 'C',
      'ć' => 'c',
      'À' => 'A',
      'Á' => 'A',
      'Â' => 'A',
      'Ã' => 'A',
      'Ä' => 'A',
      'Å' => 'A',
      'Æ' => 'A',
      'Ç' => 'C',
      'È' => 'E',
      'É' => 'E',
      'Ê' => 'E',
      'Ë' => 'E',
      'Ì' => 'I',
      'Í' => 'I',
      'Î' => 'I',
      'Ï' => 'I',
      'Ñ' => 'N',
      'Ò' => 'O',
      'Ó' => 'O',
      'Ô' => 'O',
      'Õ' => 'O',
      'Ö' => 'O',
      'Ø' => 'O',
      'Ù' => 'U',
      'Ú' => 'U',
      'Û' => 'U',
      'Ü' => 'U',
      'Ý' => 'Y',
      'Þ' => 'B',
      'ß' => 'Ss',
      'à' => 'a',
      'á' => 'a',
      'â' => 'a',
      'ã' => 'a',
      'ä' => 'a',
      'å' => 'a',
      'æ' => 'a',
      'ç' => 'c',
      'è' => 'e',
      'é' => 'e',
      'ê' => 'e',
      'ë' => 'e',
      'ì' => 'i',
      'í' => 'i',
      'î' => 'i',
      'ï' => 'i',
      'ð' => 'o',
      'ñ' => 'n',
      'ò' => 'o',
      'ó' => 'o',
      'ô' => 'o',
      'õ' => 'o',
      'ö' => 'o',
      'ø' => 'o',
      'ù' => 'u',
      'ú' => 'u',
      'û' => 'u',
      'ý' => 'y',
      'ý' => 'y',
      'þ' => 'b',
      'ÿ' => 'y',
      'Ŕ' => 'R',
      'ŕ' => 'r',
      '/' => '-',
      ' ' => '-',
    ];

    $stripped = preg_replace(['/\s{2,}/', '/[\t\n]/'], ' ', $string);

    return strtolower(strtr($stripped, $table));
  }

}
